//
//  TryGitApp.swift
//  TryGit
//
//  Created by 张丽 on 2021/1/15.
//

import SwiftUI

@main
struct TryGitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
